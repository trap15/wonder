#ifndef WSLINK_H_
#define WSLINK_H_

/* Set if section fixups ok */
#define LFLAG_SFIX_OK  1
/* Set if section has actual data */
#define LFLAG_FILL     2

#define ADR_NONE ((uint32_t)-1)

typedef struct {
  int flags;
  uint32_t vaddr; /* Address in memory map, -1 means directly after previous */ /* Only valid with SFIX_OK */
  uint32_t paddr; /* Address in binary, -1 means directly after previous */
} ws_sec_t;

enum {
  LMODE_WS_BIN = 0,
  LMODE_WSC_BIN,
  LMODE_WW_WS_BIN,
  LMODE_WW_WSC_BIN,
  LMODE_WS,
  LMODE_WSC,
  LMODE_WW_WS,
  LMODE_WW_WSC,
};

typedef struct {
  int linkmode;

  int is_color; /* 0 = WS, 1 = WSC */
  int is_witch; /* 0 = WS, 1 = WW */

  struct {
    uint8_t devid;
    uint8_t cartid;
    uint8_t save;
    uint8_t capability;
    uint8_t rtc;
  } wswan;
  struct {
    char* fname;
    char* extra;
    uint16_t flag;
  } witch;

  ws_sec_t sec_info[4];
} ws_cfg_t;

int xtoi_bs(const char* str, int base);
int xtoi(const char* str);
int xtoi_full(const char* str, int base, const char** sstr);

#endif
