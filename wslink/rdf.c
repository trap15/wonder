/* Jesus, please bless this mess */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "mem.h"
#include "wslink.h"
#include "rdf.h"

/***
 *     .d8888b. Y88b   d88P 888b     d888 88888888888 888888b.   888      
 *    d88P  Y88b Y88b d88P  8888b   d8888     888     888  "88b  888      
 *    Y88b.       Y88o88P   88888b.d88888     888     888  .88P  888      
 *     "Y888b.     Y888P    888Y88888P888     888     8888888K.  888      
 *        "Y88b.    888     888 Y888P 888     888     888  "Y88b 888      
 *          "888    888     888  Y8P  888     888     888    888 888      
 *    Y88b  d88P    888     888   "   888     888     888   d88P 888      
 *     "Y8888P"     888     888       888     888     8888888P"  88888888 
 */
void rdf_symtbl_add(rdf_t* rdf, rdf_sym_t* sym)
{
  rdf->symtab.count++;
  rdf->symtab.syms = realloc(rdf->symtab.syms, sizeof(rdf_sym_t) * rdf->symtab.count);
  rdf->symtab.syms[rdf->symtab.count-1] = *sym;
}
void rdf_symtbl_add_copy(rdf_t* rdf, rdf_sym_t* sym)
{
  rdf_sym_t nsym = *sym;
  nsym.name = strdup(nsym.name);
  rdf_symtbl_add(rdf, &nsym);
}
void rdf_symtbl_add_new(rdf_t* rdf, char* name, uint32_t addr, int seg)
{
  rdf_sym_t sym;
  if(seg == SEC_EXTERN) {
    sym.idx = addr;
    sym.rdfnum = -1;
  }else{
    sym.addr = addr;
  }
  sym.seg = seg;
  sym.name = strdup(name);

  rdf_symtbl_add(rdf, &sym);
}

uint32_t rdf_symtbl_get_extern_addr(rdf_t* rdf, int rdfnum, uint32_t idx)
{
  int i;
  for(i = 0; i < rdf->symtab.count; i++) {
    if((rdf->symtab.syms[i].rdfnum == rdfnum) &&
       (rdf->symtab.syms[i].idx == idx))
      return rdf->symtab.syms[i].addr;
  }
  return -1;
}

int rdf_symtbl_get_extern_seg(rdf_t* rdf, int rdfnum, uint32_t idx)
{
  int i;
  for(i = 0; i < rdf->symtab.count; i++) {
    if((rdf->symtab.syms[i].rdfnum == rdfnum) &&
       (rdf->symtab.syms[i].idx == idx))
      return rdf->symtab.syms[i].seg;
  }
  return -1;
}

uint32_t rdf_symtbl_get_extern_rel(rdf_t* rdf, int rdfnum, uint32_t idx)
{
  uint32_t addr;
  int seg;

  addr = rdf_symtbl_get_extern_addr(rdf, rdfnum, idx);
  seg = rdf_symtbl_get_extern_seg(rdf, rdfnum, idx);

  return rdf->secs[seg].vaddr + addr;
}

/* Attempt to find a symbol with same name that's not extern; if exists, throw an error and bail */
int rdf_symtbl_dupcheck(rdf_t* rdf)
{
  int i, l;

  for(l = 0; l < rdf->symtab.count; l++) {
    if(rdf->symtab.syms[l].seg == SEC_EXTERN)
      continue;

    for(i = l+1; i < rdf->symtab.count; i++) {
      if(rdf->symtab.syms[i].seg == SEC_EXTERN)
        continue;
      if(strcmp(rdf->symtab.syms[i].name, rdf->symtab.syms[l].name) != 0)
        continue;
      if((rdf->symtab.syms[i].seg  == rdf->symtab.syms[l].seg) &&
         (rdf->symtab.syms[i].addr == rdf->symtab.syms[l].addr))
        continue;

      fprintf(stderr, "Conflicting definitions for symbol %s! Bailing...\n", rdf->symtab.syms[l].name);
      return 0;
    }
  }
  return 1;
}

/* Resolve as many symbols as possible. */
int rdf_symtbl_resolve(rdf_t* rdf, int needs_all)
{
  int i, l;

  for(l = 0; l < rdf->symtab.count; l++) {
    if(rdf->symtab.syms[l].seg != SEC_EXTERN)
      continue;

    for(i = 0; i < rdf->symtab.count; i++) {
      if(rdf->symtab.syms[i].seg == SEC_EXTERN)
        continue;
      if(strcmp(rdf->symtab.syms[i].name, rdf->symtab.syms[l].name) != 0)
        continue;

      rdf->symtab.syms[l].seg = rdf->symtab.syms[i].seg;
      rdf->symtab.syms[l].addr = rdf->symtab.syms[i].addr;
      break;
    }
    if(i < rdf->symtab.count)
      continue;

    if(needs_all) {
      fprintf(stderr, "No matching symbol for %s! Bailing...\n", rdf->symtab.syms[l].name);
      return 0;
    }
  }

  return 1;
}

/* If true duplicate symbols exist, remove them and update references to them */
/* Non-critical, just nice for objects that aren't to be fully linked */
int rdf_symtbl_cleanup(rdf_t* rdf)
{
  int i, l;

  for(l = 0; l < rdf->symtab.count; l++) {
    if(rdf->symtab.syms[l].seg == SEC_EXTERN)
      continue;

    for(i = l+1; i < rdf->symtab.count; i++) {
      if(rdf->symtab.syms[i].seg == SEC_EXTERN)
        continue;
      if(strcmp(rdf->symtab.syms[i].name, rdf->symtab.syms[l].name) != 0)
        continue;
      /* We've already verified that we have no bad duplicates */

      /* TODO: Clean duplicate symbol */
    }
  }
  return 1;
}

/***
 *     .d8888b.  8888888888 .d8888b.  888b     d888 8888888888 888b    888 88888888888 
 *    d88P  Y88b 888       d88P  Y88b 8888b   d8888 888        8888b   888     888     
 *    Y88b.      888       888    888 88888b.d88888 888        88888b  888     888     
 *     "Y888b.   8888888   888        888Y88888P888 8888888    888Y88b 888     888     
 *        "Y88b. 888       888  88888 888 Y888P 888 888        888 Y88b888     888     
 *          "888 888       888    888 888  Y8P  888 888        888  Y88888     888     
 *    Y88b  d88P 888       Y88b  d88P 888   "   888 888        888   Y8888     888     
 *     "Y8888P"  8888888888 "Y8888P88 888       888 8888888888 888    Y888     888     
 */
void rdf_segs_add(rdf_t* rdf, rdf_seg_t* seg)
{
  rdf->nsegs++;
  rdf->segs = realloc(rdf->segs, sizeof(rdf_seg_t) * rdf->nsegs);
  rdf->segs[rdf->nsegs-1] = *seg;
}

int rdf_segs_load(rdf_t* rdf, FILE* fp)
{
  int i;
  rdf_seg_t seg;
  size_t old_pos;

  old_pos = ftell(fp);
  fseek(fp, rdf->segstart, SEEK_SET);

  rdf->nsegs = 0;
  rdf->segs = NULL;
  do {
    seg.type = readf16(fp);
    if(seg.type == 0)
      break;

    seg.num = readf16(fp);
    seg.resv = readf16(fp);
    seg.len = readf32(fp);

    seg.data = malloc(seg.len);
    fread(seg.data, seg.len, 1, fp);

    rdf_segs_add(rdf, &seg);
  } while(seg.type);

  /* Assign segments to sections */
  for(i = 0; i < rdf->nsegs; i++) {
    switch(rdf->segs[i].num) {
      case 0:
        rdf->secs[SEC_TEXT].seg = &rdf->segs[i];
        break;
      case 1:
        rdf->secs[SEC_DATA].seg = &rdf->segs[i];
        break;
    }
  }

  /* Copy relevant data from each segment to the section */
  for(i = 0; i < SEC_COUNT; i++) {
    if(rdf->secs[i].seg == NULL)
      continue;

    rdf->secs[i].len  = rdf->secs[i].seg->len;
    rdf->secs[i].data = rdf->secs[i].seg->data;
  }

  fseek(fp, old_pos, SEEK_SET);
  return 1;
}

/***
 *    8888888b.  8888888888 .d8888b.   .d88888b.  8888888b.  8888888b.  
 *    888   Y88b 888       d88P  Y88b d88P" "Y88b 888   Y88b 888  "Y88b 
 *    888    888 888       888    888 888     888 888    888 888    888 
 *    888   d88P 8888888   888        888     888 888   d88P 888    888 
 *    8888888P"  888       888        888     888 8888888P"  888    888 
 *    888 T88b   888       888    888 888     888 888 T88b   888    888 
 *    888  T88b  888       Y88b  d88P Y88b. .d88P 888  T88b  888  .d88P 
 *    888   T88b 8888888888 "Y8888P"   "Y88888P"  888   T88b 8888888P"  
 */
void rdf_recs_add(rdf_t* rdf, rdf_rec_t* rec)
{
  rdf->nrecs++;
  rdf->recs = realloc(rdf->recs, sizeof(rdf_rec_t) * rdf->nrecs);
  rdf->recs[rdf->nrecs-1] = *rec;
}

int rdf_recs_load(rdf_t* rdf, FILE* fp)
{
  int i;
  rdf_rec_t rec;
  uint8_t ssize;
  size_t old_pos, npos;

  old_pos = ftell(fp);
  fseek(fp, rdf->recstart, SEEK_SET);

  rdf->nrecs = 0;
  rdf->recs = NULL;

  while(ftell(fp) < rdf->segstart) {
    rec.type = readf8(fp);
    rec.size = readf8(fp);
    npos = ftell(fp) + rec.size;

    rec.import.name = NULL;
    rec.global.name = NULL;

    switch(rec.type) {
      case RDFREC_RELOC:
      case RDFREC_SEGRELOC:
        rec.reloc.seg = readf8(fp); /* What are the top 2 bits for? */
        rec.reloc.off = readf32(fp);
        rec.reloc.len = readf8(fp);
        rec.reloc.idx = readf16(fp);
        rec.reloc.rdfnum = -1;

        rec.reloc.base = 0;
        if(rec.reloc.seg & 0x40)
          rec.reloc.is_rel = 1;
        else
          rec.reloc.is_rel = 0;
        rec.reloc.seg &= 0x3F;
        break;

      case RDFREC_FARIMPORT:
        fprintf(stderr, "FAR IMPORT UNHANDLED!!\n");
      case RDFREC_IMPORT:
        rec.import.flag = readf8(fp);
        rec.import.idx = readf16(fp);

        ssize = rec.size - 3;
        rec.import.name = malloc(ssize + 1);
        fread(rec.import.name, ssize, 1, fp);
        rec.import.name[ssize] = '\0';
        break;

      case RDFREC_GLOBAL:
        rec.global.flag = readf8(fp);
        rec.global.seg = readf8(fp);
        rec.global.off = readf32(fp);

        ssize = rec.size - 6;
        rec.global.name = malloc(ssize + 1);
        fread(rec.global.name, ssize, 1, fp);
        rec.global.name[ssize] = '\0';
        break;

      case RDFREC_BSS:
        rec.bss.amount = readf32(fp);
        break;

      default:
        fprintf(stderr, "UNKNOWN RECORD %02X @ %lX\n", rec.type, ftell(fp));
        return 0;
    }
    rdf_recs_add(rdf, &rec);
    fseek(fp, npos, SEEK_SET);
  }

  for(i = 0; i < rdf->nrecs; i++) {
    switch(rdf->recs[i].type) {
      case RDFREC_GLOBAL:
        rdf_symtbl_add_new(rdf, rdf->recs[i].global.name, rdf->recs[i].global.off, rdf->recs[i].global.seg);
        break;
      case RDFREC_IMPORT:
        rdf_symtbl_add_new(rdf, rdf->recs[i].import.name, rdf->recs[i].import.idx, SEC_EXTERN);
        break;
      case RDFREC_BSS:
        rdf->secs[SEC_BSS].len += rdf->recs[i].bss.amount;
        break;
    }
  }

  fseek(fp, old_pos, SEEK_SET);
  return 1;
}

/***
 *    888888b.  8888888 888b    888  .d8888b.  8888888888 888b    888 
 *    888  "88b   888   8888b   888 d88P  Y88b 888        8888b   888 
 *    888  .88P   888   88888b  888 888    888 888        88888b  888 
 *    8888888K.   888   888Y88b 888 888        8888888    888Y88b 888 
 *    888  "Y88b  888   888 Y88b888 888  88888 888        888 Y88b888 
 *    888    888  888   888  Y88888 888    888 888        888  Y88888 
 *    888   d88P  888   888   Y8888 Y88b  d88P 888        888   Y8888 
 *    8888888P" 8888888 888    Y888  "Y8888P88 8888888888 888    Y888 
 */
int rdf_link_binary(rdf_t* rdf, const ws_sec_t* sec_info)
{
  int i;
  uint32_t vaddr, paddr;
  uint32_t pend;

  vaddr = 0;
  paddr = 0;
  pend = paddr;
  for(i = 0; i < SEC_COUNT; i++) {
    if(sec_info[i].vaddr != ADR_NONE)
      vaddr = sec_info[i].vaddr;
    rdf->secs[i].vaddr = vaddr;
    vaddr += rdf->secs[i].len;

    if(sec_info[i].paddr != ADR_NONE)
      paddr = sec_info[i].paddr;
    rdf->secs[i].addr = paddr;
    if(sec_info[i].flags & LFLAG_FILL)
      paddr += rdf->secs[i].len;

    if(paddr > pend)
      pend = paddr;
  }

  rdf->size = pend;
  rdf->data = malloc(rdf->size);

  for(i = 0; i < SEC_COUNT; i++) {
    if(!(sec_info[i].flags & LFLAG_FILL))
      continue;

    memcpy(rdf->data + rdf->secs[i].addr, rdf->secs[i].data, rdf->secs[i].len);
  }

#define CREATE_SEG_SYMS(sec, name) \
  if(sec_info[sec].flags & LFLAG_SFIX_OK) { \
    rdf_symtbl_add_new(rdf, "__" name "_begin", rdf->secs[sec].vaddr, 0); \
    rdf_symtbl_add_new(rdf, "__" name "_end", rdf->secs[sec].vaddr+rdf->secs[sec].len, 0); \
  } \
  rdf_symtbl_add_new(rdf, "__" name "_size", rdf->secs[sec].len, 0); \
  if(sec_info[sec].flags & LFLAG_FILL) { \
    rdf_symtbl_add_new(rdf, "__" name "_bin_begin", rdf->secs[sec].addr, 0); \
    rdf_symtbl_add_new(rdf, "__" name "_bin_end", rdf->secs[sec].addr+rdf->secs[sec].len, 0); \
  }

  CREATE_SEG_SYMS(SEC_TEXT, "text")
  CREATE_SEG_SYMS(SEC_DATA, "data")
  CREATE_SEG_SYMS(SEC_BSS, "bss")

  return 1;
}

/***
 *    8888888888 8888888 Y88b   d88P 888     888 8888888b.  
 *    888          888    Y88b d88P  888     888 888   Y88b 
 *    888          888     Y88o88P   888     888 888    888 
 *    8888888      888      Y888P    888     888 888   d88P 
 *    888          888      d888b    888     888 8888888P"  
 *    888          888     d88888b   888     888 888        
 *    888          888    d88P Y88b  Y88b. .d88P 888        
 *    888        8888888 d88P   Y88b  "Y88888P"  888        
 */
int rdf_link_fixup_reloc(rdf_t* rdf, rdf_rec_t rec, const ws_sec_t* sec_info)
{
  uint32_t dataoff;
  uint32_t rel;
  (void)sec_info;

  switch(rec.reloc.idx) {
    case SEC_TEXT: case SEC_DATA: case SEC_BSS:
      rel = rdf->secs[rec.reloc.idx].vaddr;
      break;
    default: /* Extern */
      rel = rdf_symtbl_get_extern_rel(rdf, rec.reloc.rdfnum, rec.reloc.idx);
      break;
  }

  dataoff = rec.reloc.off + rdf->secs[rec.reloc.seg].addr;
  if(rec.reloc.is_rel)
    rel -= rec.reloc.base;

  switch(rec.reloc.len) {
    case 1:
      write8(rdf->data+dataoff, read8(rdf->data+dataoff) + rel);
      break;
    case 2:
      write16(rdf->data+dataoff, read16(rdf->data+dataoff) + rel);
      break;
    case 4:
      write32(rdf->data+dataoff, read32(rdf->data+dataoff) + rel);
      break;
  }
  return 1;
}

int rdf_link_fixup_segreloc(rdf_t* rdf, rdf_rec_t rec, const ws_sec_t* sec_info)
{
  int seg;
  uint32_t dataoff;
  uint32_t rel;

  switch(rec.reloc.idx) {
    case SEC_TEXT: case SEC_DATA: case SEC_BSS:
      seg = rec.reloc.idx;
      break;
    default:
      seg = rdf_symtbl_get_extern_seg(rdf, rec.reloc.rdfnum, rec.reloc.idx);
      break;
  }
  if(!(sec_info[seg].flags & LFLAG_SFIX_OK)) {
    fprintf(stderr, "Unable to fixup segments for segment %d\n", seg);
    return 0;
  }

  rel = rdf->secs[seg].vaddr >> 4;

  dataoff = rec.reloc.off + rdf->secs[rec.reloc.seg].addr;

  switch(rec.reloc.len) {
    case 1:
      write8(rdf->data+dataoff, read8(rdf->data+dataoff) + rel);
      break;
    case 2:
      write16(rdf->data+dataoff, read16(rdf->data+dataoff) + rel);
      break;
    case 4:
      write32(rdf->data+dataoff, read32(rdf->data+dataoff) + rel);
      break;
  }
  return 1;
}

int rdf_link_fixups(rdf_t* rdf, const ws_sec_t* sec_info)
{
  int i;
  for(i = 0; i < rdf->nrecs; i++) {
    switch(rdf->recs[i].type) {
      case RDFREC_RELOC:
        if(!rdf_link_fixup_reloc(rdf, rdf->recs[i], sec_info))
          return 0;
        break;
      case RDFREC_SEGRELOC:
        if(!rdf_link_fixup_segreloc(rdf, rdf->recs[i], sec_info))
          return 0;
        break;
    }
  }
  return 1;
}

/***
 *    888      8888888 888b    888 888    d8P  8888888888 8888888b.  
 *    888        888   8888b   888 888   d8P   888        888   Y88b 
 *    888        888   88888b  888 888  d8P    888        888    888 
 *    888        888   888Y88b 888 888d88K     8888888    888   d88P 
 *    888        888   888 Y88b888 8888888b    888        8888888P"  
 *    888        888   888  Y88888 888  Y88b   888        888 T88b   
 *    888        888   888   Y8888 888   Y88b  888        888  T88b  
 *    88888888 8888888 888    Y888 888    Y88b 8888888888 888   T88b 
 */
int rdf_link(rdf_t* rdf, int count, rdf_t* rdfs, const ws_sec_t* sec_info)
{
  int i, l;

  /* Merge sections */
  for(i = 0; i < SEC_COUNT; i++) {
    for(l = 0; l < count; l++) {
      rdfs[l].secs[i].addr += rdf->secs[i].len;
      rdf->secs[i].len += rdfs[l].secs[i].len;
    }
    rdf->secs[i].data = malloc(rdf->secs[i].len);
    for(l = 0; l < count; l++) {
      if(rdfs[l].secs[i].data == NULL) continue;
      if(rdfs[l].secs[i].len == 0) continue;
      memcpy(rdf->secs[i].data + rdfs[l].secs[i].addr, rdfs[l].secs[i].data, rdfs[l].secs[i].len);
    }
  }

  for(l = 0; l < count; l++) {
    /* Adjust symbol tables */
    for(i = 0; i < rdfs[l].symtab.count; i++) {
      rdfs[l].symtab.syms[i].addr += rdfs[l].secs[rdfs[l].symtab.syms[i].seg].addr;
      rdfs[l].symtab.syms[i].rdfnum = l;
    }
    /* Adjust relocations */
    for(i = 0; i < rdfs[l].nrecs; i++) {
      if(rdfs[l].recs[i].type != RDFREC_RELOC && rdfs[l].recs[i].type != RDFREC_SEGRELOC)
        continue;
      rdfs[l].recs[i].reloc.base = rdfs[l].secs[rdfs[l].recs[i].reloc.seg].addr;
      rdfs[l].recs[i].reloc.off += rdfs[l].secs[rdfs[l].recs[i].reloc.seg].addr;
      rdfs[l].recs[i].reloc.rdfnum = l;
    }
  }

  /* Merge relocations */
  rdf->recs = NULL;
  rdf->nrecs = 0;
  for(l = 0; l < count; l++) {
    for(i = 0; i < rdfs[l].nrecs; i++) {
      if(rdfs[l].recs[i].type != RDFREC_RELOC && rdfs[l].recs[i].type != RDFREC_SEGRELOC)
        continue;
      rdf_recs_add(rdf, &rdfs[l].recs[i]);
    }
  }

  /* Merge symbol tables */
  rdf->symtab.count = 0;
  rdf->symtab.syms = NULL;
  for(l = 0; l < count; l++) {
    for(i = 0; i < rdfs[l].symtab.count; i++) {
      rdf_symtbl_add_copy(rdf, &rdfs[l].symtab.syms[i]);
    }
  }

  /* Combine together the sections to a binary and add extra final symbols */
  if(sec_info) {
    if(!rdf_link_binary(rdf, sec_info))
      return 0;
  }

  /* Check sanity of symbol table */
  if(!rdf_symtbl_dupcheck(rdf))
    return 0;

  /* Resolve EXTERN symbols */
  if(!rdf_symtbl_resolve(rdf, sec_info != NULL))
    return 0;

  /* Remove duplicate symbols */
  if(!rdf_symtbl_cleanup(rdf))
    return 0;

  /* Apply fixups */
  if(sec_info) {
    if(!rdf_link_fixups(rdf, sec_info))
      return 0;
  }

  return 1;
}

/***
 *    888      .d88888b.        d8888 8888888b.  8888888888 8888888b.  
 *    888     d88P" "Y88b      d88888 888  "Y88b 888        888   Y88b 
 *    888     888     888     d88P888 888    888 888        888    888 
 *    888     888     888    d88P 888 888    888 8888888    888   d88P 
 *    888     888     888   d88P  888 888    888 888        8888888P"  
 *    888     888     888  d88P   888 888    888 888        888 T88b   
 *    888     Y88b. .d88P d8888888888 888  .d88P 888        888  T88b  
 *    88888888 "Y88888P" d88P     888 8888888P"  8888888888 888   T88b 
 */
int rdf_load(rdf_t* rdf, const char* fn)
{
  FILE* fp;
  uint8_t buf[6];

  fp = fopen(fn, "rb");
  if(fp == NULL)
    return 0;

  fread(buf, 6, 1, fp);
  if(memcmp(buf, "RDOFF2", 6) != 0)
    return 0;

  rdf->flen = readf32(fp);
  rdf->flen += ftell(fp);

  rdf->segstart = readf32(fp);
  rdf->segstart += ftell(fp);

  rdf->recstart = ftell(fp);

  if(!rdf_segs_load(rdf, fp))
    return 0;

  if(!rdf_recs_load(rdf, fp))
    return 0;

  fclose(fp);

  return 1;
}

/***
 *    8888888 888b    888 8888888 88888888888 
 *      888   8888b   888   888       888     
 *      888   88888b  888   888       888     
 *      888   888Y88b 888   888       888     
 *      888   888 Y88b888   888       888     
 *      888   888  Y88888   888       888     
 *      888   888   Y8888   888       888     
 *    8888888 888    Y888 8888888     888     
 */
void rdf_init(rdf_t* rdf)
{
  int i;

  rdf->symtab.count = 0;
  rdf->symtab.syms = NULL;
  for(i = 0; i < SEC_COUNT; i++) {
    rdf->secs[i].addr = 0;
    rdf->secs[i].len = 0;
    rdf->secs[i].data = NULL;
    rdf->secs[i].seg = NULL;
  }

  rdf->size = 0;
  rdf->data = NULL;

  rdf->nsegs = 0;
  rdf->segs = NULL;

  rdf->nrecs = 0;
  rdf->recs = NULL;

  rdf->flen = rdf->segstart = rdf->recstart = 0;
}

/***
 *    8888888b.  8888888888 888      8888888888 88888888888 8888888888 
 *    888  "Y88b 888        888      888            888     888        
 *    888    888 888        888      888            888     888        
 *    888    888 8888888    888      8888888        888     8888888    
 *    888    888 888        888      888            888     888        
 *    888    888 888        888      888            888     888        
 *    888  .d88P 888        888      888            888     888        
 *    8888888P"  8888888888 88888888 8888888888     888     8888888888 
 */
void rdf_delete(rdf_t* rdf)
{
  int i;

  if(rdf->data != NULL)
    free(rdf->data);

  if(rdf->symtab.syms) {
    for(i = 0; i < rdf->symtab.count; i++) {
      free(rdf->symtab.syms[i].name);
    }
    free(rdf->symtab.syms);
  }

  for(i = 0; i < SEC_COUNT; i++) {
    if(rdf->secs[i].seg)
      continue;
    if(rdf->secs[i].data)
      free(rdf->secs[i].data);
  }

  if(rdf->segs) {
    for(i = 0; i < rdf->nsegs; i++) {
      if(rdf->segs[i].data)
        free(rdf->segs[i].data);
    }
    free(rdf->segs);
  }

  if(rdf->recs) {
    for(i = 0; i < rdf->nrecs; i++) {
      if(rdf->recs[i].import.name)
        free(rdf->recs[i].import.name);
      if(rdf->recs[i].global.name)
        free(rdf->recs[i].global.name);
    }
    free(rdf->recs);
  }
}
