#ifndef RDF_H_
#define RDF_H_

enum {
  SEC_TEXT = 0,
  SEC_DATA,
  SEC_BSS,
  SEC_EXTERN,
  SEC_COUNT,
};

#define MAX_SYMNAME 64

#define RDFREC_GENERIC   0
#define RDFREC_RELOC     1
#define RDFREC_IMPORT    2
#define RDFREC_GLOBAL    3
#define RDFREC_DLL       4
#define RDFREC_BSS       5
#define RDFREC_SEGRELOC  6
#define RDFREC_FARIMPORT 7
#define RDFREC_MODNAME   8
#define RDFREC_COMMON    10

typedef struct {
  char* name;
  uint32_t addr;
  int seg;

  int rdfnum;
  uint32_t idx;
} rdf_sym_t;

typedef struct {
  int count;
  rdf_sym_t* syms;
} rdf_symtab_t;

typedef struct {
  int type;
  uint16_t num;
  uint16_t resv;
  uint32_t len;

  size_t offset;

  void* data;
} rdf_seg_t;

typedef struct {
  uint32_t addr;
  uint32_t vaddr;

  uint32_t len;
  uint8_t* data;

  rdf_seg_t* seg;
} rdf_sec_t;

typedef struct {
  uint8_t type, size;

  struct {
    uint8_t seg;
    uint32_t off;
    uint8_t len;
    uint16_t idx;
    uint16_t rdfnum;
    int is_rel;
    uint32_t base;
  } reloc;

  struct {
    uint8_t flag;
    uint16_t idx;
    char* name;
  } import;

  struct {
    uint8_t flag;
    uint8_t seg;
    uint32_t off;
    char* name;
  } global;

  struct {
    uint32_t amount;
  } bss;
} rdf_rec_t;

typedef struct {
  uint32_t flen;
  uint32_t segstart;
  uint32_t recstart;

  rdf_sec_t secs[SEC_COUNT];

  int nsegs;
  rdf_seg_t* segs;

  int nrecs;
  rdf_rec_t* recs;

  rdf_symtab_t symtab;

  uint32_t size;
  uint8_t* data;
} rdf_t;

int rdf_load(rdf_t* rdf, const char* fn);
int rdf_link(rdf_t* rdf, int count, rdf_t* rdfs, const ws_sec_t* sec_info);

void rdf_init(rdf_t* rdf);
void rdf_delete(rdf_t* rdf); /* Doesn't free pointer */

#endif
