#ifndef MEM_H_
#define MEM_H_

uint8_t read8(void* data);
uint16_t read16(void* data);
uint32_t read32(void* data);

void write8(void* data, uint8_t val);
void write16(void* data, uint16_t val);
void write32(void* data, uint32_t val);

uint8_t readf8(FILE* fp);
uint16_t readf16(FILE* fp);
uint32_t readf32(FILE* fp);

void writef8(FILE* fp, uint8_t val);
void writef16(FILE* fp, uint16_t val);
void writef32(FILE* fp, uint32_t val);

#endif
