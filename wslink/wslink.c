#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "wslink.h"
#include "mem.h"
#include "rdf.h"
#include "ini.h"

const char* app;
void usage(void)
{
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s mode conffile outfile infiles...\n", app);
  fprintf(stderr, "\nmode is composed of 3 sections:\n");
  fprintf(stderr, "\t[ww_][dev][_bin]\n");
  fprintf(stderr, "Where ww_ is added if building for WonderWitch, and _bin is added if building\n");
  fprintf(stderr, "the final binary. dev is either ws or wsc, to specify WonderSwan or \n");
  fprintf(stderr, "WonderSwan Color.\n");
}

void free_config(ws_cfg_t* cfg)
{
  if(cfg->witch.fname)
    free(cfg->witch.fname);
  if(cfg->witch.extra)
    free(cfg->witch.extra);
}

#define INIMATCH(s, n) ((strcmp(section, s) == 0) && (strcmp(name, n) == 0))
static int wsconf_handler(void* user, const char* section, const char* name, const char* value)
{
  ws_cfg_t* cfg = user;
  const char* p;
  /* Wonderwitch configs */
  if(INIMATCH("wonderwitch", "filename")) {
    if(cfg->witch.fname) free(cfg->witch.fname);
    cfg->witch.fname = strdup(value);
  }else if(INIMATCH("wonderwitch", "extra")) {
    if(cfg->witch.extra) free(cfg->witch.extra);
    cfg->witch.extra = strdup(value);
  }else if(INIMATCH("wonderwitch", "flag")) {
    /* TODO: Make this support strings */
    cfg->witch.flag = xtoi(value);
  }else
  /* Wonderswan configs */
  if(INIMATCH("wonderswan", "devid")) {
    cfg->wswan.devid = xtoi(value);
  }else if(INIMATCH("wonderswan", "cartid")) {
    cfg->wswan.cartid = xtoi(value);
  }else if(INIMATCH("wonderswan", "save")) {
    /* TODO: Make this support strings */
    cfg->wswan.save = xtoi(value);
  }else if(INIMATCH("wonderswan", "orient")) {
    for(p = value; *p != '\0'; p++) {
      switch(*p) {
        case 'v': case 'V':
          cfg->wswan.capability |= 1;
          break;
        case 'h': case 'H':
          cfg->wswan.capability |= 2;
          break;
      }
    }
  }else if(INIMATCH("wonderswan", "rtc")) {
    cfg->wswan.rtc = xtoi(value) ? 1 : 0;
  }

  return 1;
}

static int wsld_handle_section(ws_cfg_t* cfg, int binnum, const char* binsec, const char* section, const char* name, const char* value)
{
  if(INIMATCH(binsec, "paddr")) {
    if(value[0] != '+')
      cfg->sec_info[binnum].paddr = xtoi(value);
    return 1;
  }else if(INIMATCH(binsec, "vaddr")) {
    if(value[0] != '+')
      cfg->sec_info[binnum].vaddr = xtoi(value);
    return 1;
  }else if(INIMATCH(binsec, "pic")) {
    if(strcmp(value, "yes") == 0) {
      cfg->sec_info[binnum].flags &= ~LFLAG_SFIX_OK;
    }else if(strcmp(value, "no") == 0) {
      cfg->sec_info[binnum].flags |=  LFLAG_SFIX_OK;
    }
    return 1;
  }else if(INIMATCH(binsec, "fill")) {
    if(strcmp(value, "yes") == 0) {
      cfg->sec_info[binnum].flags |=  LFLAG_FILL;
    }else if(strcmp(value, "no") == 0) {
      cfg->sec_info[binnum].flags &= ~LFLAG_FILL;
    }
    return 1;
  }
  return 0;
}
static int wsld_handler(void* user, const char* section, const char* name, const char* value)
{
  ws_cfg_t* cfg = user;
  if(wsld_handle_section(cfg, 0, ".text", section, name, value))
    return 1;
  else if(wsld_handle_section(cfg, 1, ".data", section, name, value))
    return 1;
  else if(wsld_handle_section(cfg, 2, ".bss", section, name, value))
    return 1;
  else
    printf("Unknown LD entry %s.%s=%s\n", section, name, value);

  return 0;
}

int handle_config(ws_cfg_t* cfg, const char* mode, const char* cfgfn)
{
  if(strcasecmp(mode, "ws_bin") == 0) {
    cfg->linkmode = LMODE_WS_BIN;
  }else if(strcasecmp(mode, "wsc_bin") == 0) {
    cfg->linkmode = LMODE_WSC_BIN;
  }else if(strcasecmp(mode, "ww_ws_bin") == 0) {
    cfg->linkmode = LMODE_WW_WS_BIN;
  }else if(strcasecmp(mode, "ww_wsc_bin") == 0) {
    cfg->linkmode = LMODE_WW_WSC_BIN;
  }else if(strcasecmp(mode, "ws") == 0) {
    cfg->linkmode = LMODE_WS;
  }else if(strcasecmp(mode, "wsc") == 0) {
    cfg->linkmode = LMODE_WSC;
  }else if(strcasecmp(mode, "ww_ws") == 0) {
    cfg->linkmode = LMODE_WW_WS;
  }else if(strcasecmp(mode, "ww_wsc") == 0) {
    cfg->linkmode = LMODE_WW_WSC;
  }else{
    cfg->linkmode = -1;
  }

  /* Defaults */
  cfg->witch.flag = 7;
  cfg->witch.fname = NULL;
  cfg->witch.extra = NULL;

  cfg->wswan.devid = 0;
  cfg->wswan.cartid = 0;
  cfg->wswan.save = 0;
  cfg->wswan.capability = 4;
  cfg->wswan.rtc = 0;

  switch(cfg->linkmode) {
    default:
    case LMODE_WS:
    case LMODE_WW_WS:
    case LMODE_WS_BIN:
    case LMODE_WW_WS_BIN:
      cfg->is_color = 0;
      break;
    case LMODE_WSC:
    case LMODE_WW_WSC:
    case LMODE_WSC_BIN:
    case LMODE_WW_WSC_BIN:
      cfg->is_color = 1;
      break;
  }

  switch(cfg->linkmode) {
    default:
    case LMODE_WS:
    case LMODE_WSC:
    case LMODE_WS_BIN:
    case LMODE_WSC_BIN:
      cfg->is_witch = 0;
      break;
    case LMODE_WW_WS:
    case LMODE_WW_WSC:
    case LMODE_WW_WS_BIN:
    case LMODE_WW_WSC_BIN:
      cfg->is_witch = 1;
      break;
  }

  if(ini_parse(cfgfn, wsconf_handler, cfg) < 0) {
    fprintf(stderr, "Error parsing configuration\n");
    return 0;
  }

  return 1;
}

int handle_ld_config(ws_cfg_t* cfg, const char* ldfn)
{
  int i;
  for(i = 0; i < 4; i++) {
    cfg->sec_info[i].flags = 0;
    cfg->sec_info[i].vaddr = ADR_NONE;
    cfg->sec_info[i].paddr = ADR_NONE;
  }

  if(ini_parse(ldfn, wsld_handler, cfg) < 0) {
    fprintf(stderr, "Error parsing ld config\n");
    return 0;
  }

  return 1;
}

int do_link(ws_cfg_t* cfg, const char* outfn, const char* ldfn, int count, const char** infns)
{
  int i;
  FILE* fp;
  rdf_t* rdfs;

  rdf_t mrdf; /* Master RDF */

  if(!handle_ld_config(cfg, ldfn))
    return 0;

  rdfs = malloc(sizeof(rdf_t) * count);

  for(i = 0; i < count; i++) {
    rdf_init(&rdfs[i]);
    if(!rdf_load(&rdfs[i], infns[i]))
      return 0;
  }

  rdf_init(&mrdf);

  if(!rdf_link(&mrdf, count, rdfs, cfg->sec_info))
    return 0;

  for(i = 0; i < count; i++) {
    rdf_delete(&rdfs[i]);
  }
  free(rdfs);

  fp = fopen(outfn, "wb");
  if(fp == NULL)
    return 0;
  fwrite(mrdf.data, mrdf.size, 1, fp);
  fclose(fp);

  rdf_delete(&mrdf);

  return 1;
}

static int rom_size_table[6] = {
  2, /* 4 Mbit */
  3, /* 8 Mbit */
  4, /* 16 Mbit */
  6, /* 32 Mbit */
  8, /* 64 Mbit */
  9, /* 128 Mbit */
};

int do_ws_image(ws_cfg_t* cfg, const char* outfn, const char* infn)
{
  FILE* fp;
  uint8_t* bin;
  size_t size, nsize, v, pos;
  uint16_t xsum;

  fp = fopen(infn, "rb");
  if(fp == NULL)
    return 0;
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  v = (size + 25) - 1; /* Adjust for header space */
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;

  if(v < 4*128*1024) /* Under 4Mbit needs push up */
    v = 4*128*1024;
  if(v > 128*128*1024) /* Over 128Mbit needs push down */
    v = 128*128*1024;

  nsize = v;

  v >>= 19;
  for(pos = 0; v; pos++) v >>= 1;

  if(nsize < size) size = nsize;

  bin = malloc(nsize);
  memset(bin, 0xFF, nsize);
  fread(bin, size, 1, fp);
  fclose(fp);

  /* Entrypoint */
  write8(bin + nsize-16+0, 0xEA); write32(bin + nsize-16+1, 0xF000FFE7); /* jmp far 0F000:FFE7h */
  write8(bin + nsize-32+7+0, 0x31); write8(bin + nsize-32+7+1, 0xC0); /* xor ax,ax */
  write8(bin + nsize-32+7+2, 0xE6); write8(bin + nsize-32+7+3, 0xC2); /* out 0C2h, ax */
  write8(bin + nsize-32+7+4, 0xEA); write32(bin + nsize-32+7+5, 0x20000000); /* jmp far 02000:0000h */

  write8(bin + nsize-10+0, cfg->wswan.devid);
  write8(bin + nsize-10+1, cfg->is_color);
  write8(bin + nsize-10+2, cfg->wswan.cartid);
  write8(bin + nsize-10+4, rom_size_table[pos]);
  write8(bin + nsize-10+5, cfg->wswan.save);
  write8(bin + nsize-10+6, cfg->wswan.capability);
  write8(bin + nsize-10+7, cfg->wswan.rtc);

  xsum = 0;
  for(pos = 0; pos < nsize-2; pos++) {
    xsum += read8(bin + pos);
  }
  write16(bin + nsize-10+8, xsum);

  fp = fopen(outfn, "wb");
  if(fp == NULL)
    return 0;
  fwrite(bin, nsize, 1, fp);
  fclose(fp);

  free(bin);

  return 1;
}

int do_ww_image(ws_cfg_t* cfg, const char* outfn, const char* infn)
{
  FILE* fp;
  uint8_t* bin;
  size_t size;

  fp = fopen(infn, "rb");
  if(fp == NULL)
    return 0;
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  bin = malloc(size + 0x80);
  memset(bin, 0xFF, size + 0x80);
  fread(bin+0x80, size, 1, fp);
  fclose(fp);

  write8(bin+0, '#'); write8(bin+1, '!'); write8(bin+2, 'w'); write8(bin+3, 's');

  memset(bin+0x40, 0, 0x28);
  if(cfg->witch.fname)
    strncpy((char*)bin+0x40, cfg->witch.fname, 0x10);
  if(cfg->witch.extra)
    strncpy((char*)bin+0x50, cfg->witch.extra, 0x18);

  /* Offset */
  write32(bin+0x68, 0);
  /* Size */
  write32(bin+0x6C, size);
  /* Number of blocks */
  write16(bin+0x70, (size+0xFF) >> 7);
  /* Mode flags */
  write16(bin+0x72, cfg->witch.flag);
  /* Modify time */
  write32(bin+0x74, 0);
  /* Handler */
  write32(bin+0x78, 0);
  /* Resource */
  write32(bin+0x7C, 0xFFFFFFFF);

  fp = fopen(outfn, "wb");
  fwrite(bin, size+0x80, 1, fp);
  fclose(fp);

  free(bin);

  return 1;
}

int main(int argc, const char* argv[])
{
  const char* cfgfn;
  const char* outfn;
  const char* mode;
  const char* ldfn;
  int in_count;
  const char** infns;
  ws_cfg_t cfg;

  printf("WSLink (C)2014 Alex 'trap15' Marshall\n");
  app = argv[0];
  if(argc < 4) {
    fprintf(stderr, "Not enough arguments.\n");
    usage();
    return EXIT_FAILURE;
  }

  mode = argv[1];
  cfgfn = argv[2];
  outfn = argv[3];
  in_count = argc - 4;
  infns = argv + 4;

  if(!handle_config(&cfg, mode, cfgfn))
    return EXIT_FAILURE;

  switch(cfg.linkmode) {
    case LMODE_WS_BIN:
    case LMODE_WSC_BIN:
    case LMODE_WW_WS_BIN:
    case LMODE_WW_WSC_BIN:
      ldfn = argv[3];
      outfn = argv[4];
      in_count = argc - 5;
      infns = argv + 5;

      if(argc < 5) {
        fprintf(stderr, "Not enough arguments.\n");
        usage();
        return EXIT_FAILURE;
      }

      if(!do_link(&cfg, outfn, ldfn, in_count, infns))
        return EXIT_FAILURE;
      break;
    case LMODE_WS:
    case LMODE_WSC:
      if(!do_ws_image(&cfg, outfn, infns[0]))
        return EXIT_FAILURE;
      break;
    case LMODE_WW_WS:
    case LMODE_WW_WSC:
      if(!do_ww_image(&cfg, outfn, infns[0]))
        return EXIT_FAILURE;
      break;
    default:
      fprintf(stderr, "Invalid mode.\n");
      usage();
      return EXIT_FAILURE;
  }

  free_config(&cfg);

  return EXIT_SUCCESS;
}

int xtoi_full(const char* str, int base, const char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  const char* ostr;

  ostr = str;
  if(base == -1) base = 10;

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val;
}

int xtoi_bs(const char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(const char* str) { return xtoi_full(str, -1, NULL); }
