# Files

* `wonder.mk` - A base makefile to be included from a regular Makefile that will
  handle compilation, linking and generation of final binaries.

* `ww_init.asm` - A source file containing the real entrypoint for all
  WonderWitch applications generated from wonder.mk. Copies .data and clears
  .bss, then passes a `start` to the loader. `start` sets up the stack, and
  calls `main`. Returning from `main` will run the `bios_exit` WW syscall.

* `ws_init.asm` - A source file containing the real entrypoint for all
  WonderSwan applications generated from wonder.mk. Copies .data and clears
  .bss, sets up the stack, assigns sane ROM banks, then calls `main`. Returning
  from `main` will cause an infinite loop.
