#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "wshead.h"
#include "mem.h"
#include "ini.h"

const char* app;
void usage(void)
{
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s [ws|wsc|wwsft] conffile outfile infile\n", app);
}

void free_config(ws_cfg_t* cfg)
{
  (void)cfg;
}

#define INIMATCH(s, n) ((strcmp(section, s) == 0) && (strcmp(name, n) == 0))
static int wsconf_handler(void* user, const char* section, const char* name, const char* value)
{
  ws_cfg_t* cfg = user;
  const char* p;
  /* Wonderswan configs */
  if(INIMATCH("wonderswan", "devid")) {
    cfg->wswan.devid = xtoi(value);
  }else if(INIMATCH("wonderswan", "cartid")) {
    cfg->wswan.cartid = xtoi(value);
  }else if(INIMATCH("wonderswan", "save")) {
    /* TODO: Make this support strings */
    cfg->wswan.save = xtoi(value);
  }else if(INIMATCH("wonderswan", "orient")) {
    for(p = value; *p != '\0'; p++) {
      switch(*p) {
        case 'v': case 'V':
          cfg->wswan.capability |= 1;
          break;
        case 'h': case 'H':
          cfg->wswan.capability |= 2;
          break;
      }
    }
  }else if(INIMATCH("wonderswan", "rtc")) {
    cfg->wswan.rtc = xtoi(value) ? 1 : 0;
  }

  return 1;
}

int handle_config(ws_cfg_t* cfg, const char* mode, const char* cfgfn)
{
  cfg->ww_soft = 0;
  if(strcasecmp(mode, "ws") == 0) {
    cfg->is_color = 0;
  }else if(strcasecmp(mode, "wsc") == 0) {
    cfg->is_color = 1;
  }else if(strcasecmp(mode, "wwsft") == 0) {
    cfg->is_color = 1;
    cfg->ww_soft = 1;
  }else{
    cfg->is_color = -1;
  }

  /* Defaults */
  cfg->wswan.devid = 0;
  cfg->wswan.cartid = 0;
  cfg->wswan.save = 0;
  cfg->wswan.capability = 4;
  cfg->wswan.rtc = 0;

  if(ini_parse(cfgfn, wsconf_handler, cfg) < 0) {
    fprintf(stderr, "Error parsing configuration\n");
    return 0;
  }

  return 1;
}

static int rom_size_table[6] = {
  2, /* 4 Mbit */
  3, /* 8 Mbit */
  4, /* 16 Mbit */
  6, /* 32 Mbit */
  8, /* 64 Mbit */
  9, /* 128 Mbit */
};

int do_ws_image(ws_cfg_t* cfg, const char* outfn, const char* infn)
{
  FILE* fp;
  uint8_t* bin;
  size_t size, nsize, v, pos;
  uint16_t xsum;

  fp = fopen(infn, "rb");
  if(fp == NULL)
    return 0;
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);

  v = (size + 16) - 1; /* Adjust for header space */
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;

  if(v < 4*128*1024) /* Under 4Mbit needs push up */
    v = 4*128*1024;
  if(v > 128*128*1024) /* Over 128Mbit needs push down */
    v = 128*128*1024;

  if(cfg->ww_soft)
    v = 448*1024;

  nsize = v;

  v >>= 20;
  for(pos = 0; v; pos++) v >>= 1;

  if(nsize < size) size = nsize;

  bin = malloc(nsize);
  memset(bin, 0xFF, nsize);
  fread(bin, size, 1, fp);
  fclose(fp);

  /* Entrypoint */
  write8(bin + nsize-16+0, 0xEA); write32(bin + nsize-16+1, 0x80000000); /* jmp far 08000:0000h */

  write8(bin + nsize-16+5, 0);
  write8(bin + nsize-10+0, cfg->wswan.devid);
  write8(bin + nsize-10+1, cfg->is_color);
  write8(bin + nsize-10+2, cfg->wswan.cartid);
  write8(bin + nsize-10+3, 0);
  write8(bin + nsize-10+4, rom_size_table[pos]);
  write8(bin + nsize-10+5, cfg->wswan.save);
  write8(bin + nsize-10+6, cfg->wswan.capability);
  write8(bin + nsize-10+7, cfg->wswan.rtc);

  xsum = 0;
  for(pos = 0; pos < nsize-2; pos++) {
    xsum += read8(bin + pos);
  }
  write16(bin + nsize-10+8, xsum);

  fp = fopen(outfn, "wb");
  if(fp == NULL)
    return 0;
  fwrite(bin, nsize, 1, fp);
  fclose(fp);

  free(bin);

  return 1;
}

int main(int argc, const char* argv[])
{
  const char* cfgfn;
  const char* outfn;
  const char* mode;
  const char* infn;
  ws_cfg_t cfg;

  printf("WSHead (C)2014-2015 Alex 'trap15' Marshall\n");
  app = argv[0];
  if(argc < 4) {
    fprintf(stderr, "Not enough arguments.\n");
    usage();
    return EXIT_FAILURE;
  }

  mode = argv[1];
  cfgfn = argv[2];
  outfn = argv[3];
  infn = argv[4];

  if(!handle_config(&cfg, mode, cfgfn))
    return EXIT_FAILURE;

  if(!do_ws_image(&cfg, outfn, infn))
    return EXIT_FAILURE;

  free_config(&cfg);

  return EXIT_SUCCESS;
}

int xtoi_full(const char* str, int base, const char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  const char* ostr;

  ostr = str;
  if(base == -1) base = 10;

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val;
}

int xtoi_bs(const char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(const char* str) { return xtoi_full(str, -1, NULL); }
