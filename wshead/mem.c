#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "mem.h"

uint8_t read8(void* data)
{
  uint8_t* buf = data;
  return *buf;
}
uint16_t read16(void* data)
{
  uint8_t* buf = data;
  return read8(buf) | (read8(buf+1) << 8);
}
uint32_t read32(void* data)
{
  uint8_t* buf = data;
  return read16(buf) | (read16(buf+2) << 16);
}

void write8(void* data, uint8_t val)
{
  uint8_t* buf = data;
  *buf = val;
}
void write16(void* data, uint16_t val)
{
  uint8_t* buf = data;
  write8(buf, val);
  write8(buf+1, val >> 8);
}
void write32(void* data, uint32_t val)
{
  uint8_t* buf = data;
  write16(buf, val);
  write16(buf+2, val >> 16);
}

uint8_t readf8(FILE* fp)
{
  uint8_t val;
  fread(&val, 1, 1, fp);
  return val;
}
uint16_t readf16(FILE* fp)
{
  uint16_t val;
  val = readf8(fp);
  val|= readf8(fp) << 8;
  return val;
}
uint32_t readf32(FILE* fp)
{
  uint32_t val;
  val = readf16(fp);
  val|= readf16(fp) << 16;
  return val;
}

void writef8(FILE* fp, uint8_t val)
{
  fwrite(&val, 1, 1, fp);
}
void writef16(FILE* fp, uint16_t val)
{
  writef8(fp, val >> 0);
  writef8(fp, val >> 8);
}
void writef32(FILE* fp, uint32_t val)
{
  writef16(fp, val >> 0);
  writef16(fp, val >> 16);
}
