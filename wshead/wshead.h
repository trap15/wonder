#ifndef WSHEAD_H_
#define WSHEAD_H_

typedef struct {
  int is_color; /* 0 = WS, 1 = WSC */
  int ww_soft;

  struct {
    uint8_t devid;
    uint8_t cartid;
    uint8_t save;
    uint8_t capability;
    uint8_t rtc;
  } wswan;
} ws_cfg_t;

int xtoi_bs(const char* str, int base);
int xtoi(const char* str);
int xtoi_full(const char* str, int base, const char** sstr);

#endif
