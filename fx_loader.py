#!/usr/bin/env python
"""
fx_loader.py
"""

FX_MAGIC = "#!ws"

import struct
import sys
import os
try:
	from awesome_print import ap as pp
except:
	from pprint import pprint as pp
try:
	from idaapi import *
	from idautils import *
	from idc import *
	IN_IDA = True
except:
	print "not running in IDA?"
	IN_IDA = False

class FXImage:
	"""
	typedef struct {
		UINT32 magic;
		UINT8  pad0[0x3C];
		UINT8  shortname[0x10];
		UINT8  longname[0x10];
		UINT8  pad1[0xC];
		UINT32 size;
		UINT16 unk0;
		UINT16 unk1;
		UINT32 xsum;
		UINT32 pad2;
		UINT32 pad3;
	} WWitchFX;
	"""

	def __init__(self, f):
		self.offset = f.tell()

		# read header
		(self.magic, self.pad0, self.shortname, self.longname,
			self.pad1, self.size, self.unk0, self.unk1,
			self.xsum, self.pad2, self.pad3) = struct.unpack("<L60s16s16s12sLHHLLL", f.read(0x80))


# ida entry point
def accept_file(f, n):
	retval = 0

	if n == 0:
		f.seek(0)
		if f.read(4) == FX_MAGIC:
			retval = "WonderWitch executable"

	return retval

# ida entry point
def load_file(f, neflags, format):
	# parse header
	f.seek(0)
	fx = FXImage(f)

	# load binary
	f.file2base(0, 0, 0x80+fx.size, True)
	add_segm(0, 0, 0x80, ".head", "DATA")
	set_segm_addressing(get_segm_by_name(".head"), 0)
	add_segm(0x8, 0x80, 0x80+fx.size, ".text", "CODE")
	set_segm_addressing(get_segm_by_name(".text"), 0)
	add_segm(0x10000, 0x100000, 0x110000, ".sram", "DATA")
	set_segm_addressing(get_segm_by_name(".sram"), 0)
	add_segm(0x11000, 0x110000, 0x120000, ".ram", "DATA")
	set_segm_addressing(get_segm_by_name(".ram"), 0)

	add_entry(0x80, 0x80, "entrypoint", 1)

	SetProcessorType("80286r", SETPROC_ALL)

	return 1
