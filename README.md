# Wonder

Wonder is a collection of tools and code for developing for the
Bandai WonderSwan and WonderWitch.

# Contents

Wonder contains an SDK, samples, and some tools. It also comes with some
documentation of the WonderSwan and WonderWitch hardware.

## SDK

Wonder contains what is essentially an SDK. Look to the documentation for more
information.

## Samples

Sample projects that use this SDK are included in `samples/`.
`samples/template/` contains a barebones project that should be suitable for
expansion.

## Tools

Wonder contains the following tools:

* `WSLink` - Links NASM RDOFF objects and creates binaries suitable for use on
  a WonderSwan flash cart or emulator, or a WonderWitch, depending
  on parameters. Special configuration is provided via a project-specific
  configuration file.

* `MakeFX` - Creates a .FX file for WonderWitch.

* `fx_loader.py` - A loader for IDA Pro that will load WonderWitch .FX
  binaries.

## Documentation

Documentation is included in the following locations:

* `doc/hw` contains documentation on the base WonderSwan hardware.
* `doc/freya` contains documentation on WonderWitch, including the OS and BIOS.
* `doc/wonder` contains documentation on the Wonder SDK.

# Cool Links

* <http://daifukkat.su/> - My website
* <https://bitbucket.org/trap15/wonder> - This repository
* <http://wonderwitch.qute.co.jp/> - Official WonderWitch website
* <http://emudocs.org/WonderSwan/wstech24.txt> - WStech 2.4 by dox and Judge
* <http://www.nasm.us/xdoc/2.11.02/html/nasmdoc0.html> - NASM documentation

# Greetz

* LiraNuna - Helping me out big-time, and pushing me to buy a WonderWitch ;D
* Ryphecha - Always helpful, and Mednafen is fantastic
* rancor - Your ultra-cheap WonderSwan has ruined my life
* \#raidenii - Forever impossible
* Gunpei Yokoi - A true idol, may he rest in peace.
* Qute Corporation - WonderWitch is a fantastic set of HW/SW, well done.
* Bandai
* French Roast Coffee

# Licensing

All contents within this repository (unless otherwise specified) are licensed
under the following terms:

The MIT License (MIT)

Copyright (c) 2014 Alex 'trap15' Marshall

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
