	BITS	16
	CPU	186

; Procedure header
%macro	proc	0-1 near
%push	proc%1

%ifidn %1,near
%stacksize large
%elifidn %1,far
%stacksize small
%else
%error "Invalid proc type"
%endif
%assign %$localsize 0
%assign %$has_frame 0

%define %$procname %00

	GLOBAL	%00

%00:
%endmacro

; Procedure footer
%macro	endp	0
%if %$has_frame == 1
	leave
%endif
%ifidn %00,remove_label
%endif
%ifctx procnear
	retn
%elifctx procfar
	retf
%else
%error "Mismatched `endproc'/`proc'"
%endif

%pop
%endmacro

; Procedure argument
%macro	arg 1-*
%rep %0
	%arg	%1
%rotate 1
%endrep
%endmacro

; Procedure local
%macro	local 1-*
%rep %0
	%local	%1
%rotate 1
%endrep
%endmacro

%macro	endpvar	0
%assign	%$has_frame 1
	enter	%$localsize, 0
%endmacro
