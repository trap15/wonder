; Segments
%define	RAM_SEG		0000h
%define	SRAM_SEG	1000h
%define	ROM0_SEG	2000h
%define	ROM1_SEG	3000h
%define	ROM2_SEG	4000h

; Bank
%define	BANK_ROM2	0C0h
%define	BANK_SRAM	0C1h
%define	BANK_ROM0	0C2h
%define	BANK_ROM1	0C3h

; Key
%define	KEY_PORT	0B5h

; Interrupts
%define	HWINT_BASE	0B0h
%define	HWINT_EN	0B2h
%define	HWINT_ACK	0B6h

%define	HWINT_SER_TX	00h
%define	HWINT_KEY	01h
%define	HWINT_CART	02h
%define	HWINT_SER_RX	03h
%define	HWINT_LINE	04h
%define	HWINT_VBLK_TMR	05h
%define	HWINT_VBLK	06h
%define	HWINT_HBLK_TMR	07h
