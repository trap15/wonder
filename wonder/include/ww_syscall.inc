%ifndef DEVICE_WW
	%error	"WonderWitch syscalls aren't present on WonderSwan!"
%endif

%define INT_EXIT	10h
%define INT_KEY		11h
%define INT_DISP	12h
%define INT_TEXT	13h
%define INT_COMM	14h
%define INT_SOUND	15h
%define INT_TIMER	16h
%define INT_SYS		17h
%define INT_BANK	18h

%define KEY_PRESS_CHECK			00h
%define KEY_HIT_CHECK			01h
%define KEY_WAIT			02h
%define KEY_SET_REPEAT			03h
%define KEY_GET_REPEAT			04h
%define KEY_HIT_CHECK_WITH_REPEAT	05h
