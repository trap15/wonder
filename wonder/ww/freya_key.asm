key_press_check proc
	mov	ah, KEY_PRESS_CHECK
	int	INT_KEY
key_press_check endp

key_hit_check proc
	mov	ah, KEY_HIT_CHECK
	int	INT_KEY
key_hit_check endp

key_wait proc
	mov	ah, KEY_WAIT
	int	INT_KEY
key_wait endp

key_set_repeat proc
	arg	rate:word, delay:word
	endpvar

	mov	bl, [rate]
	mov	bh, [delay]
	mov	ah, KEY_SET_REPEAT
	int	INT_KEY
key_set_repeat endp

key_get_repeat	proc
	mov	ah, KEY_GET_REPEAT
	int	INT_KEY
key_get_repeat	endp

key_hit_check_with_repeat proc
	mov	ah, KEY_HIT_CHECK_WITH_REPEAT
	int	INT_KEY
key_hit_check_with_repeat endp
