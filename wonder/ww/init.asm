%ifndef DEVICE_WW
	%error	"Using ww_init when not building for a WonderWitch"
%endif

%include "ws_base.inc"
%include "ws_hw.inc"
%include "ww_syscall.inc"

	EXTERN	__data_begin
	EXTERN	__data_bin_begin
	EXTERN	__data_size
	EXTERN	__bss_begin
	EXTERN	__bss_size

	EXTERN	bios_exit

	EXTERN	main
	GLOBAL	entrypoint

	SECTION	.text
entrypoint:
	push	ds
	push	di
	push	si
	push	cx
	push	bx

	; Copy .data to SRAM
	mov	cx, __data_size
	shr	cx, 1
	test	cx, 0FFFFh
	jz	.end_data_copy

	push	cs
	pop	ds

	mov	ax, SRAM_SEG
	mov	es, ax
	mov	si, __data_bin_begin
	mov	di, __data_begin
	rep movsw

.end_data_copy:
	; Clear .bss
	mov	cx, __bss_size
	shr	cx, 1
	test	cx, 0FFFFh
	jz	.end_bss_clear

	mov	ax, SRAM_SEG
	mov	es, ax
	mov	di, __bss_begin
	xor	ax, ax
	rep stosw

.end_bss_clear:

	; Return start
	mov	dx, cs
	mov	ax, start

	pop	bx
	pop	cx
	pop	si
	pop	di
	pop	ds

	retf

start:
	mov	ax, RAM_SEG
	mov	ss, ax
	mov	sp, 02000h

	call	main

	call	bios_exit

	ret
