%ifndef DEVICE_WW
	%error	"Using ww_freya when not building for a WonderWitch"
%endif

%include "ws_base.inc"
%include "ws_hw.inc"
%include "ww_syscall.inc"

	SECTION	.text
%include "ww/freya_exit.asm"
%include "ww/freya_key.asm"
