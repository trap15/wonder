%ifndef DEVICE_WS
	%error	"Using ws_freya when not building for a WonderSwan"
%endif

%include "ws_base.inc"
%include "ws_hw.inc"

	SECTION	.text
%include "ws/freya_exit.asm"
%include "ws/freya_key.asm"
