key_press_check proc
	xor	dx,dx
	mov	al, 0100b << 4
	out	KEY_PORT, al
	daa	; Delay 10 clocks
	in	al, KEY_PORT
	and	al, 0Fh
	mov	dl, al

	mov	al, 0010b << 4
	out	KEY_PORT, al
	daa	; Delay 10 clocks
	in	al, KEY_PORT
	and	al, 0Fh
	shl	al, 4
	or	dl, al

	mov	al, 0001b << 4
	out	KEY_PORT, al
	daa	; Delay 10 clocks
	in	al, KEY_PORT
	and	al, 0Fh
	mov	dh, al

	mov	al, 0111b << 4
	out	KEY_PORT, al
	mov	ax, [key_cur]
	xor	ax, dx
	mov	[key_xorlast], ax
	mov	ax, dx
	mov	[key_cur], ax
key_press_check endp

key_hit_check proc
	call	key_press_check
	and	ax, [key_xorlast]
	jz	.end_hit

	; Handle repeat
	push	ax
		mov	[key_hit], ax
		mov	al, [key_repeat_delay]
		mov	[key_repeat_cur], al
	pop	ax
.end_hit:
	and	ax, ax
key_hit_check endp

; TODO: Support repeat
key_wait proc
.loop:
	call	key_hit_check
	jnz	.done
	hlt
	jmp	.loop
.done:
key_wait endp

key_set_repeat proc
	arg	rate:word, delay:word
	endpvar

	mov	al, [rate]
	mov	[key_repeat_rate], al
	mov	al, [delay]
	mov	[key_repeat_delay], al
key_set_repeat endp

key_get_repeat proc
	mov	al, [key_repeat_rate]
	mov	ah, [key_repeat_delay]
key_get_repeat endp

key_hit_check_with_repeat proc
	call	key_hit_check
	jnz	.done
	; TODO: Support repeat
.done:
key_hit_check_with_repeat endp

	SECTION	.data
key_cur:	dw	0
key_xorlast:	dw	0
key_hit:	dw	0

key_repeat_rate:	db	0
key_repeat_delay:	db	0
key_repeat_cur:		db	0
