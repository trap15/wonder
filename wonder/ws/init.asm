%ifndef DEVICE_WS
	%error	"Using ws_init when not building for a WonderSwan"
%endif

%include "ws_base.inc"
%include "ws_hw.inc"

	EXTERN	__data_begin
	EXTERN	__data_bin_begin
	EXTERN	__data_size
	EXTERN	__bss_begin
	EXTERN	__bss_size

	EXTERN	main
	GLOBAL	entrypoint

	SECTION	.text
entrypoint:
	cli
	; Copy .data to RAM
	mov	cx, __data_size
	shr	cx, 1
	test	cx, 0FFFFh
	jz	.end_data_copy

	push	cs
	pop	ds

	mov	ax, RAM_SEG
	mov	es, ax
	mov	si, __data_bin_begin
	mov	di, __data_begin
	rep movsw

.end_data_copy:
	; Clear .bss
	mov	cx, __bss_size
	shr	cx, 1
	test	cx, 0FFFFh
	jz	.end_bss_clear

	mov	ax, RAM_SEG
	mov	es, ax
	mov	di, __bss_begin
	xor	ax, ax
	rep stosw

.end_bss_clear:

	mov	ax, RAM_SEG
	mov	ds, ax

	mov	ax, RAM_SEG
	mov	ss, ax
	mov	sp, 02000h

	mov	ax, 01h
	out	BANK_ROM1, ax

	xor	ax, ax
	out	BANK_ROM2, ax

	xor	al, al
	out	HWINT_BASE, al
	out	HWINT_EN, al
	not	al
	out	HWINT_ACK, al

	xor	bx, bx
	mov	cx, 40h
.int_init_loop:
	mov	word [bx], null_int
	mov	word [bx+2], cs
	add	bx, 4
	loop	.int_init_loop

	mov	al, 01000010b
	out	HWINT_EN, al

	sti

	call	main

.endloop:
	hlt
	jmp	.endloop


null_int:
	iret
