# Check if we know where we are.
# Maybe it's possible to autodetect?
ifeq ($(origin WONDER), undefined)
	$(error WONDER is undefined, it should point to the location of wonder)
endif

# Setup defaults if necessary
NASM	?= nasm
WSLINK	?= wslink

DEBUG	?= 0
DEVICE	?= wonderswan
MODEL   ?= wsc
SFLAGS	?=
WSCONF	?= wsconf.cfg

FREYA	?= 0

# Verify input parameters
ifeq ($(origin OUTPUT), undefined)
	$(error OUTPUT is undefined)
endif
ifeq ($(origin OBJECTS), undefined)
	$(error No OBJECTS defined)
endif

ifneq ($(MODEL),wsc)
	ifneq ($(MODEL),ws)
		$(error MODEL must be either 'wsc' or 'ws')
	endif
endif

ifneq ($(DEVICE),wonderwitch)
	ifneq ($(DEVICE),wonderswan)
		$(error DEVICE must be either 'wonderswan' or 'wonderwitch')
	endif
endif

# Required SFLAGS.
SFLAGS	+= -f rdf -i $(WONDER)/ -i $(WONDER)/include/

# Handle the DEBUG parameter
ifeq ($(DEBUG),1)
SFLAGS += -dDEBUG
endif

# Handle the MODEL parameter
ifeq ($(MODEL),wsc)
OUTFMT	:= wsc
SFLAGS	+= -dMODEL_WSC
else ifeq ($(MODEL),ws)
OUTFMT	:= ws
SFLAGS	+= -dMODEL_WS
endif
OBJPFX	:= $(MODEL)

WSFILE	:= $(OUTPUT).$(OUTFMT)

# Handle the DEVICE parameter
ifeq ($(DEVICE),wonderwitch)
OUTFILE	:= $(OUTPUT).fx
DEVNAME	:= ww
OBJPFX	:= ww_$(OBJPFX)
SFLAGS	+= -dDEVICE_WW
else ifeq ($(DEVICE),wonderswan)
OUTFILE	:= $(WSFILE)
DEVNAME	:= ws
SFLAGS	+= -dDEVICE_WS
endif

WSLD	?= $(WONDER)/$(OBJPFX).ld

BINFILE	:= obj/$(OBJPFX)_$(OUTPUT).bin

PROJOBJ	:= $(addprefix obj/$(OBJPFX)/,$(OBJECTS))
WONDOBJ	:= $(WONDER)/$(DEVNAME)/init.obj
ifeq ($(FREYA),1)
WONDOBJ	+= $(WONDER)/$(DEVNAME)/freya.obj
SFLAGS	+= -dFREYA
endif

OBJECTS	:= $(WONDOBJ) $(PROJOBJ)

CLEANS	:= $(WSFILE) $(BINFILE) $(OUTPUT).fx $(PROJOBJ)
DCLEANS	:= $(CLEANS) obj/*_$(OUTPUT).bin

.PHONY: all clean distclean

all: $(OUTFILE) $(BINFILE) $(OBJECTS)

obj/*/%.obj: src/%.asm
	$(NASM) $(SFLAGS) -o $@ $<
$(WONDER)/%.obj: $(WONDER)/%.asm
	$(NASM) $(SFLAGS) -o $@ $<
$(OUTFILE): $(BINFILE)
	$(WSLINK) $(OBJPFX) $(WSCONF) $@ $<
%.bin: $(OBJECTS)
	$(WSLINK) $(OBJPFX)_bin $(WSCONF) $(WSLD) $@ $+
clean:
	rm -f $(CLEANS)
distclean:
	rm -f $(DCLEANS)
