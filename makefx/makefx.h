#ifndef MAKEFX_H_
#define MAKEFX_H_

typedef struct {
  char* fname;
  char* extra;
  uint16_t flag;
} ws_cfg_t;

int xtoi_bs(const char* str, int base);
int xtoi(const char* str);
int xtoi_full(const char* str, int base, const char** sstr);

#endif
