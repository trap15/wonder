#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "makefx.h"
#include "mem.h"
#include "ini.h"

const char* app;
void usage(void)
{
  fprintf(stderr, "Usage:\n");
  fprintf(stderr, "\t%s conffile outfile infile\n", app);
}

void free_config(ws_cfg_t* cfg)
{
  if(cfg->fname)
    free(cfg->fname);
  if(cfg->extra)
    free(cfg->extra);
}

#define INIMATCH(s, n) ((strcmp(section, s) == 0) && (strcmp(name, n) == 0))
static int wsconf_handler(void* user, const char* section, const char* name, const char* value)
{
  ws_cfg_t* cfg = user;
  /* Wonderwitch configs */
  if(INIMATCH("fx", "filename")) {
    if(cfg->fname) free(cfg->fname);
    cfg->fname = strdup(value);
  }else if(INIMATCH("fx", "extra")) {
    if(cfg->extra) free(cfg->extra);
    cfg->extra = strdup(value);
  }else if(INIMATCH("fx", "flag")) {
    /* TODO: Make this support strings */
    cfg->flag = xtoi(value);
  }

  return 1;
}

int handle_config(ws_cfg_t* cfg, const char* cfgfn, const char* infn)
{
  /* Defaults */
  cfg->flag = 7;
  cfg->fname = strdup(infn);
  cfg->extra = NULL;

  if(ini_parse(cfgfn, wsconf_handler, cfg) < 0) {
    fprintf(stderr, "Error parsing configuration\n");
    return 0;
  }

  return 1;
}

int do_fx_image(ws_cfg_t* cfg, const char* outfn, const char* infn)
{
  FILE* fp;
  uint8_t* bin;
  size_t size;

  fp = fopen(infn, "rb");
  if(fp == NULL)
    return 0;
  fseek(fp, 0, SEEK_END);
  size = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  bin = malloc(size + 0x80);
  memset(bin, 0xFF, size + 0x80);
  fread(bin+0x80, size, 1, fp);
  fclose(fp);

  write8(bin+0, '#'); write8(bin+1, '!'); write8(bin+2, 'w'); write8(bin+3, 's');

  memset(bin+0x40, 0, 0x28);
  if(cfg->fname && cfg->extra) {
    strncpy((char*)bin+0x40, cfg->fname, 0x10);
    strncpy((char*)bin+0x50, cfg->extra, 0x18);
  }else if(cfg->fname) {
    strncpy((char*)bin+0x40, cfg->fname, 0x10);
    strncpy((char*)bin+0x50, cfg->fname, 0x18);
  }else if(cfg->extra) {
    strncpy((char*)bin+0x40, cfg->extra, 0x10);
    strncpy((char*)bin+0x50, cfg->extra, 0x18);
  }

  /* Offset */
  write32(bin+0x68, 0);
  /* Size */
  write32(bin+0x6C, size);
  /* Number of blocks */
  write16(bin+0x70, (size+0xFF) >> 7);
  /* Mode flags */
  write16(bin+0x72, cfg->flag);
  /* Modify time */
  write32(bin+0x74, 0);
  /* Handler */
  write32(bin+0x78, 0);
  /* Resource */
  write32(bin+0x7C, 0xFFFFFFFF);

  fp = fopen(outfn, "wb");
  fwrite(bin, size+0x80, 1, fp);
  fclose(fp);

  free(bin);

  return 1;
}

int main(int argc, const char* argv[])
{
  const char* cfgfn;
  const char* outfn;
  const char* infn;
  ws_cfg_t cfg;

  printf("MakeFX (C)2014 Alex 'trap15' Marshall\n");
  app = argv[0];
  if(argc != 4) {
    fprintf(stderr, "Invalid arguments.\n");
    usage();
    return EXIT_FAILURE;
  }

  cfgfn = argv[1];
  outfn = argv[2];
  infn = argv[3];

  if(!handle_config(&cfg, cfgfn, infn))
    return EXIT_FAILURE;

  if(!do_fx_image(&cfg, outfn, infn))
    return EXIT_FAILURE;

  free_config(&cfg);

  return EXIT_SUCCESS;
}

int xtoi_full(const char* str, int base, const char** sstr)
{
  int val = 0;
  int done, hit;
  char c;
  const char* ostr;

  ostr = str;
  if(base == -1) base = 10;

  switch(*str) {
    case '0':
      str++;
      switch(*str) {
        case 'x':
          base = 16;
          str++;
          break;
        case 'b':
          base = 2;
          str++;
          break;
        case 'o':
          base = 8;
          str++;
          break;
        default:
          str--;
          break;
      }
      break;
    case '$':
      base = 16;
      str++;
      break;
  }

  hit = 0;
  done = 0;
  while(!done) {
    c = *str++;
    switch(c) {
      case 'a': case 'b':
      case 'c': case 'd':
      case 'e': case 'f':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'a') + 0xA;
        break;
      case 'A': case 'B':
      case 'C': case 'D':
      case 'E': case 'F':
        if(base < 16) {
          done = 1;
          break;
        }
        val *= base;
        val += (c - 'A') + 0xA;
        break;
      case '8': case '9':
        if(base < 10) {
          done = 1;
          break;
        }
        /* fall thru */
      case '2': case '3':
      case '4': case '5':
      case '6': case '7':
        if(base < 8) {
          done = 1;
          break;
        }
        /* fall thru */
      case '0': case '1':
        val *= base;
        val += c - '0';
        break;
      default:
        done = 1;
        break;
    }
    if(!done) hit = 1;
  }

  if(!hit) {
    str = ostr+1;
    val = 0;
  }

  if(sstr) *sstr = str-1;
  return val;
}

int xtoi_bs(const char* str, int base) { return xtoi_full(str, base, NULL); }
int xtoi(const char* str) { return xtoi_full(str, -1, NULL); }
